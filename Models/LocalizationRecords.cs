﻿using System;
using System.Collections.Generic;

namespace LocalizationApi.Models
{
    public partial class LocalizationRecords
    {
        public long Id { get; set; }
        public string Key { get; set; }
        public string ResourceKey { get; set; }
        public string Text { get; set; }
        public string LocalizationCulture { get; set; }
        public string UpdatedTimestamp { get; set; }
    }
}
