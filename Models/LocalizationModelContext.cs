﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace LocalizationApi.Models
{
    public partial class LocalizationModelContext : DbContext
    {
        public LocalizationModelContext()
        {
        }

        public LocalizationModelContext(DbContextOptions<LocalizationModelContext> options)
            : base(options)
        {
        }

        public virtual DbSet<LocalizationRecords> LocalizationRecords { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LocalizationRecords>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.UpdatedTimestamp).IsRequired();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
