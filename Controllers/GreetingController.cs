﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Localization;

namespace LocalizationApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GreetingController : Controller
    {
        private readonly IStringLocalizer<GreetingController> _localizer;

        public GreetingController(IStringLocalizer<GreetingController> localizer)
        {
            _localizer = localizer;
        }

        [HttpGet]
        public string Get()
        {
            return _localizer["Greeting"];
        }
    }
}
