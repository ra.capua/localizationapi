using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using Localization.SqlLocalizer.DbStringLocalizer;

namespace LocalizationApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Get connection string
            var connectionString = Configuration["Database:ConnectionString"];
            services.AddDbContext<LocalizationModelContext>(options =>
                options.UseSqlite(
                    connectionString,
                    b => b.MigrationsAssembly("LocalizationApi")
                ),
                ServiceLifetime.Singleton,
                ServiceLifetime.Singleton
            );

            services.AddSqlLocalization();

            // Add supported cultures
            services.Configure<RequestLocalizationOptions>(options => {
               var supportedCultures = new List<CultureInfo> {
                   new CultureInfo("en-US"),
                   new CultureInfo("es-ES"),
                   new CultureInfo("ja"),
                   new CultureInfo("fr-FR"),
                   new CultureInfo("de-DE"),
               };

               options.DefaultRequestCulture = new RequestCulture(culture: "en-US", uiCulture: "en-US");
               options.SupportedCultures = supportedCultures;
               options.SupportedUICultures = supportedCultures;
            });

            services.AddControllers()
                .AddViewLocalization()
                .AddDataAnnotationsLocalization();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var locOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(locOptions.Value);
            
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
