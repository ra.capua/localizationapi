Greeting API that demonstrates localization using an SQL database.

### Requirements

  - [.NET Core 3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1)

### How to run

Open a terminal in the repo folder and run `dotnet run`. You can view the app through [http://localhost:5000/api/greeting](http://localhost:5000/api/greeting).

### How to use

Append `?culture=[locale code]` to the URL to display the same greeting in a different language. Supported languages (locale codes in parentheses) are: 

  - English (en-US)
  - Spanish (es-ES)
  - Japanese (ja)
  - French (fr-FR)
  - German (de-DE)